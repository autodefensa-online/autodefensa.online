% Huelga internacional trans★feminista ante el despojo digital

[español](https://hangar.org/8m/index.es.html) 
[français](https://pad.constantvzw.org/p/JourContreCloud) 
[Deutsch](https://pad.riseup.net/p/transfeministischerstreikgegendigitale-keep) 
[Nederlands](https://pad.vvvvvvaria.org/8m-nl)
[عربي]()
[português](https://etherdump.vvvvvvaria.org/publish/8mDigiDepletionStrike-PT.raw.html) 
[ελληνικά](https://pad.vvvvvvaria.org/8m.gr)
[română](https://pad.vvvvvvaria.org/8m_translation_RO)
[italiano](https://pad.vvvvvvaria.org/8m-it)

<!-- do not change the main strike convocation :-) 
Para generar el achivo para alojar en tu web: 
    
$ curl https://pad.vvvvvvaria.org/8m_es/export/txt | pandoc --from markdown --to html --standalone -o index.html

Necesitas tener el paquete instalado también:

 $ apt install pandoc

Pega el index.html en una carpeta /8m en tu webroot

Thanks so much for joining the web ring!
-->

## Miércoles 8 de marzo de 2023 ✨

El 8 de marzo de 2023, convocamos un día de acción contra la Nube.

En este día, intentaremos abstenernos de usar, alimentar o cuidar La Gran Nube Tecnológica. La huelga reivindica una reducción drástica de los servicios digitales extractivos y una organización colectiva abundante. Nos unimos a la larga cola histórica de huelgas feministas internacionales, porque entendemos que esta lucha tiene que ver con el trabajo, los cuidados, el antirracismo, la vida queer y la tecno-política trans★feminista.

Demasiados aspectos de la vida dependen de La Nube. Los modos expansionistas, extractivistas y financiarizados del Big Tech convierten todos los procesos vivos y creativos en beneficios. Esto afecta profundamente a cómo organizamos y cuidamos los recursos. Muchas instituciones públicas como hospitales, universidades, archivos y escuelas han pasado a alquilar software como servicio para sus operaciones básicas. Los intereses del Big Tech condicionan cómo enseñamos, hacemos accesibles, aprendemos, conocemos, organizamos, trabajamos, amamos, dormimos, nos comunicamos, administramos, cuidamos y recordamos.

Especialmente ahora que nuestra dependencia de la gran Nube parece intratable, es hora de reclamar espacio para renegociar lo que podría ser posible. Queremos imaginar diferentes infraestructuras para la vida colectiva con y sin computación. Al hacer un llamamiento a la resistencia a la nube, queremos centrarnos en prácticas de servidores que sean lentas, transfeministas, antirracistas y antiimperialistas. Queremos almacenamiento digital local, videollamadas autoalojadas y alojamiento colaborativo de servidores. Queremos infras antifas, gráficos de bajo consumo y circuitos queer. Queremos desarrollo accesible, mantenimiento tecnológico sostenible y cadenas de suministro asilvestradas. Queremos el fin del trabajo condicionado por la Gran Tecnología y, en última instancia, el fin del trabajo. Queremos un cambio sistémico, alegre y tecno-político.

Nos movilizamos desde muchos lugares: proyectos autogestionados, centros comunitarios, instituciones públicas, organizaciones culturales, empresas privadas, cooperativas y otras constelaciones. El 8 de marzo inventaremos, propondremos, traduciremos y reflejaremos modos locales de acción contra las nubes. El plan es festejar en las ruinas del Big Tech mientras descendemos y disentimos de la nube. Ese día experimentaremos con la reducción al mínimo del uso de aplicaciones basadas en la nube, debatiremos las implicaciones del régimen de la nube, documentaremos el agotamiento de los recursos comunitarios por parte de la infraestructura de las grandes tecnologías, recordaremos a nuestras organizaciones que organicen las infraestructuras digitales en nuestro interés, soñaremos con métodos alternativos de supervivencia exuberante y alegre, e imaginaremos redes locales para modos transnacionales de comunicación y funcionamiento en solidaridad transversal.
Esta huelga está convocada por:
    
<!-- please correct, add names, and questions, answers, resources below: -->

Esta huelga está convocada por:

[Anarchaserver](https://alexandria.anarchaserver.org) (Calafou), [Bidston Observatory Artistic Research Centre](https://bidstonobservatory.org) (Liverpool), [Ren Loren Britton](https://www.lorenbritton.com) (Berlin), [Constant](https://constantvzw.org) (Brussels), [Critical Data Studies Program](https://bachelor.au.dk/en/supplementary-subject/criticaldatastudies) (Aarhus University), [Dept. of Autonomous Design](https://schoolofartsgent.be), KASK (Ghent), [esc mkl](https://esc.mur.at) (Graz), [Hangar](https://hangar.org) (Barcelona), [In-grid]( https://www.in-grid.io) (London), [The Institute for Technology in The Public Interest](https://titipi.org) (Basel, Brussels, London), [NEoN](https://neondigitalarts.com) (Dundee), time of tribes (Edinburgh), [Varia](https://varia.zone) (Rotterdam), A Video Store After the End of the World (Copenhagen), [Systerserver](https://systerserver.net/), [servus.at](https://servus.at) (Linz), [Hackers and Designers](https://hackersanddesigners.nl/) (Amsterdam), [la_bekka - espacio hackfeminista](https://labekka.red/) (Canary Island & Argentina), [Red Autodefensa Feminista Online](https://autodefensa.online/) (Spain), ...

Para adherirte a la huelga, individual, colectiva o institucionalmente, subscríbete a esta lista de correo: <https://boucan.domainepublic.net/mailman3/postorius/lists/8m.lists.constantvzw.org/>

## Preguntas frecuentes ✨

### ¿Qué es la nube?

La Nube es un término para el poder computacional que se gestiona de forma centralizada, con la promesa de optimizar por flexibilidad o agilidad. La Nube reúne enormes cantidades de ordenadores, y luego ofrece trozos más pequeños y más grandes de computación como un servicio. La nube a la que queremos resistirnos está gestionada por grandes empresas tecnológicas como Amazon, Google y Microsoft, que alquilan capacidad de procesamiento a otras empresas u organizaciones.

La nube de las grandes tecnológicas no son solo los ordenadores de otres. Consolida un paradigma de software que cuenta con un software como servicio continuamente actualizado, con una infraestructura computacional escalable y montones de smartphones, y la economía política de las empresas Big Tech que cotizan en bolsa. Los beneficios del Big Tech Cloud se basan en la extracción de energía, minerales y mano de obra racializada.

### ¿Qué pasa con la nube?

Como las empresas de la nube (Amazon, Alphabet, Meta, Microsoft) son propiedad de accionistas, tienen que demostrar que crecen año tras año. Esto significa que siempre deben ser más eficientes y rápidas, para que sus accionistas obtengan beneficios. Una vez implantados los procesos digitales, es necesario ampliarlos continuamente a nuevas áreas, lo que aumenta la necesidad de más computación, o más servicios en la nube. Lo que se suele llamar ¡MÁS COMPUTACIÓN!

Los servicios en la nube están diseñados para ampliarse. Esto significa que siempre esperamos más de ellos, en términos de disponibilidad, velocidad y fiabilidad. A medida que las infraestructuras tecnológicas se expanden, cada vez son menos las cosas de las que podemos ocuparnos nosotres mismes, como comunidad (por ejemplo, vemos grupos que intentan ejecutar servidores de correo electrónico sin servicios en la nube, y les resulta casi imposible debido a la complejidad/velocidad, etc. esperada). Ya no es una opción desarrollar, implementar o incluso elegir tecnologías digitales soberanas y modos de mantenimiento autónomos.

Los particulares, las instituciones públicas y los colectivos dependen de estos servicios para sus operaciones básicas. A medida que los departamentos de informática cierran uno tras otro, nuestras habilidades, deseos y capacidades se convierten en recursos a extraer, mientras la ágil lógica de las grandes tecnologías transforma la sanidad, la educación, el activismo e incluso los encuentros sexoafectivos. Las prácticas inventivas, fundamentadas y poco ortodoxas se agotan o se vuelven inimaginables. Es un círculo vicioso: cuanto más incapaces somos de cuidar las herramientas que utilizamos a diario, más dependemos de las empresas para que nos den respuestas incluso a cuestiones sencillas. Y las tecnologías que se necesitan para la accesibilidad y como herramientas de organización de la discapacidad, una y otra vez, no centran las necesidades de quienes sólo pueden participar con sus operaciones.

Este falso paradigma de eficiencia nos vuelve inútiles y destruye nuestra capacidad de recuperación. Como “usuarie” no hay que preocuparse, nuestro deseo de ser eficientes está gestionado y no necesitamos pensar. Reducir a los sujetos y las comunidades a “usuarios” o “grupos de usuarios” nos distrae de condiciones más densas y complejas.

Los gobiernos cuentan cada vez más con La Nube, y a menudo colaboran con Big Tech para proporcionar infraestructuras materiales esenciales como redes eléctricas, suministro de agua, carreteras, terrenos, cables, etcétera. La Nube Big Tech agota las infraestructuras públicas y los bienes públicos, pero evita pagar impuestos.

Se necesitan enormes cantidades de energía y materiales para hacer funcionar las granjas de servidores: los productos químicos tóxicos para limpiar el agua de refrigeración, los minerales para los chips y componentes y los metales para los bastidores de los servidores se producen en condiciones de explotación laboral. Enormes cantidades de energía, suministrada por combustibles fósiles o energías renovables extractivas a gran escala, los mantienen en funcionamiento. Esta extracción ahonda las rutas coloniales y depende de mano de obra racializada.

### ¿No contribuye La Nube al Net Zero?

Aunque la computación consume muchos recursos, la nube promete hacerlo con menos impacto medioambiental. La promesa de Net Zero se ha convertido en un argumento seductor para que las organizaciones se pasen a La Nube, especialmente ahora que los organismos que las financian les piden que demuestren cómo reducen las emisiones de carbono. Pero la posibilidad de un colapso climático, y los sentimientos de dolor y desesperación que pueden ser necesarios para transformar nuestras acciones, se borran con este tipo de soluciones sólo administrativas. Las grandes tecnológicas proponen que la única forma de abordar las necesidades globales es a través de la ampliación, en lugar de construir solidaridad transnacional.

Queremos que las instituciones públicas rindan cuentas y se transformen activamente. Y también necesitamos estructuras solidarias responsables que se apoyen mutuamente para realizar pequeños cambios locales con posibilidades reales de transformación también a escala global. Confiar en las grandes empresas tecnológicas privatizadas para encontrar soluciones a las emisiones de carbono no funcionará.

### ¿Por qué hay tantas organizaciones (culturales) implicadas en esta huelga?

La mayoría de las organizadoras iniciales son organizaciones con un compromiso a largo plazo con la política cultural cotidiana trans*feminista y tecnocientífica. Han compartido sus preocupaciones sobre la apropiación de la práctica cultural por parte de las grandes empresas tecnológicas en diversas conversaciones, colaboraciones y redes entrelazadas.

Pero la huelga no depende únicamente de las instituciones públicas. Las instituciones públicas también sostienen estados-nación o estructuras más amplias de gobierno de la población. Significa más bien que estamos dispuestes a movilizarnos tanto por el lado de reclamar una participación en lo que la infraestructura tiene lugar a nuestro alrededor, así como rechazar la forma en que las instituciones contribuyen al régimen moderno, colonial, comercial, estatal y patriarcal.

### ¿Por qué una huelga de despojo digital el 8 de marzo?

Las infraestructuras de cuidado y mantenimiento son cuestiones planteadas por las históricas huelgas feministas de cuidado, y estas cuestiones se aplican con fuerza también a las infraestructuras tecnológicas.

El feminismo es y siempre será anticapitalista por defecto. Todas las objeciones y giros a esa afirmación son defensas de una mundialización que ocurre explícitamente contra y a pesar de modos de existencia disidentes, excluidos y/o minoritarios. Por eso, derribar la nube tiene que ser un horizonte fundamental de las luchas trans*feministas contemporáneas.

### Qué significa "trans*feminista"?

Esta huelga se denomina "trans*feminista" para resaltar algunos de los aspectos interseccionales e intraseccionales necesarios en torno a la estrella (*). El término engrosa la complejidad de los feminismos, en solidaridad con las luchas por el trabajo, los cuidados, el antirracismo, el abilismo, el edadismo, la vida queer y la tecnopolítica. En culturas no anglosajonas, especialmente en contextos de habla hispana, se utiliza el término "trans*feminista" en lugar del término inglés "queer" que a menudo queda sin traducir, y por tanto excluyente. Dado que organizamos la huelga el 8 de marzo, un día que tiene una fuerte tradición binaria y esencialista, nos pareció especialmente importante ser explícitas sobre la transversalidad general de las luchas, así como ponernos del lado de las luchas trans específicamente.

### ¿Cómo sé si estoy utilizando La Nube?

La mayoría de las cosas que se hacen en línea hoy en día, dependen de alguna forma de la Gran Nube Tecnológica.

#### Cosas que dependen del procesamiento en la nube:

* Todo lo que haces con tu Smartphone, o cuando te suscribes a un servicio en línea (como Zoom, Spotify, Netflix o WhatsApp).
* Cualquier acuerdo al que llegues tú o tu organización para almacenar archivos y acceder a ellos a distancia (como fotos o documentos, por ejemplo: Wetransfer, Dropbox, Google Drive).
* Procesos de la cadena de suministro gestionados digitalmente (como pedidos a través de Amazon o entregas a través de DHL).
* Cualquier cosa que intentes hacer fuera de la nube pero en comunicación con otra persona cuyos servicios dependan de un servicio en la nube (como usar una dirección de Gmail).

#### Cosas (digitales) que no dependen de La Gran Nube Tecnológica:

* Información almacenada y procesos ejecutados en servidores internos, en ordenadores distintos en otro lugar o en discos duros y memorias USB.
* Los protocolos de comunicación, como el envío y recepción de correo electrónico, no tienen por qué depender de la nube, pero podrían hacerlo (por ejemplo, Gmail).
* Acceso a un archivo en un servidor local con un navegador

Nextcloud (¡sic!) y Big Blue Button son aplicaciones de Software Libre que permiten a la gente alojar sus propias instancias, por lo que no forman parte de la Nube de las Grandes Tecnologías. Pero aunque estas herramientas formen parte de economías políticas y ecosistemas muy diferentes, siguen el paradigma del software como servicio, y a veces (no siempre) están alojadas en Amazon o Google Cloud.


## Más rescursos ✨

* [Digital Solidarity Networks](https://vvvvvvaria.org/etherpump/p/digital-solidarity-networks.raw.html)
* [A Wishlist for Trans*Feminist Servers](https://www.bakonline.org/prospections/a-wishlist-for-transfeminist-servers/)
* [Toward a Minor Tech]( https://openresearch.lsbu.ac.uk/item/931wz or https://darc.au.dk/fileadmin/DARC/newspapers/toward-a-minor-tech-online-sm.pdf)
* [A Catalogue of Formats for Digital Discomfort](https://titipi.org/projects/discomfort/CatalogOFFDigitalDiscomfort.pdf)
* [Infrastructural Interactions Workbook](https://titipi.org/wiki/index.php/Infrastructural_Interactions_Workbook) + [plain text version](https://titipi.org/wiki/index.php/Unfolding:Infrastructural_Interactions)
* [Infrables](https://titipi.org/pub/Infrables.pdf) + [plain text version](https://titipi.org/wiki/index.php/Infrable-collection)
* [Counter Cloud Action Plan](https://titipi.org/pub/Counter_Cloud_Action_Plan.pdf) + [plain text version](https://titipi.org/wiki/index.php/Counter_Cloud_Action_Plan)
* [Readings about Feminist Infrastructure](https://alexandria.anarchaserver.org/index.php/Feminist_Infrastructure)
* [Systerserver's peertube](https://tube.systerserver.net/)
* ...

## Acerca de esta página web ✨

Esta página web multicabecera está alojada y mantenida asíncronamente por una red de redes que incluye: [Constant](https://constantvzw.org) (Brussels), [The Institute for Technology in the Public Interest](http://titipi.org) (Basel, London, Brussels), [Varia](https://varia.zone) (Rotterdam), [Hangar](https://hangar.org/8m/) (Barcelona),  and [rosa](https://hub.vvvvvvaria.org/rosa/) (travelling), [Bidston Observatory](https://bidstonobservatory.org/8m) (Liverpool), [In-grid](https://www.in-grid.io/8m/) (London), [NeON](https://neondigitalarts.com/) (Dundee), [esc mkl](https://esc.mur.at/) (Graz), [Anarchaserver](https://alexandria.anarchaserver.org) (Calafou), [Systerserver](https://systerserver.net/), [Digitalcare.noho.st](https://digitalcare.noho.st) (Antwerp), [Hackers & Designers](https://drop.hackersanddesigners.nl/8m/) (Amsterdam), [la_bekka - espacio hackfeminista](https://labekka.red/) (Canary Island & Argentina), [Red Autodefensa Feminista Online](https://autodefensa.online/) (Spain)....

* <https://www.in-grid.io/8m/>
* <https://neondigitalarts.com/8m>
* <https://bidstonobservatory.org/8m>
* <https://titipi.org/8m/>
* <https://constantvzw.org/8m/>
* <https://varia.zone/8m/>
* <https://anarchaserver.org/8m/>
* <https://hangar.org/8m/>
* <https://hub.vvvvvvaria.org/rosa/8m/>
* <https://systerserver.net/8m/>
* <https://digitalcare.noho.st/site/8m/>
* <https://drop.hackersanddesigners.nl/8m/>
* <https://labekka.red/8m/>
* <https://autodefensa.online/8m>
* ...



<style>
@font-face { 
    font-family: "crickx";
    src: url("Crickx.woff");
}
@font-face {
    font-family: "Compagnon";
    src: url("fengardoneue_regular-webfont.woff");
}
body {

    margin: 2em;

    width: 60%;

    font-family: 'Compagnon', sans-serif;

    font-size: 18px;

    /*line-height: 1.25em;*/

    padding-left: 1.6em;

    padding-bottom: 2em;

    padding-top: 1.5em;

    margin: 0px auto;

}
p {
    padding: 0.2em;
    padding-left: 4em;
}
li {
    list-style: none;
    padding-left: 1.6em;
}
li::before {
    content: "💫 ";
}
h1, h2, h3, h4 {
    font-family: "crickx", sans-serif;
    padding: 0.4em;
}
a {
    color: black;
}

@media only screen and (max-width: 875px) {
    body {
        width: 100%;
        padding-left: 0;
    }
    p, h1, h2, h3, h4 {
        padding: 1rem;
    }
    li {
        padding-left: 0;
    }
}

</style>

<!-- add your url below -->

<script type="text/javascript">
    var next = ['https://titipi.org/8m/', 'https://constantvzw.org/8m/', 'https://varia.zone/8m/', 'https://anarchaserver.org/8m/', 'https://hub.vvvvvvaria.org/rosa/8m/', 'https://www.in-grid.io/8m/', 'https://systerserver.net/8m/', 'https://digitalcare.noho.st/site/8m/', 'https://drop.hackersanddesigners.nl/8m/', 'https://hangar.org/8m/', 'https://labekka.red/8m/', 'https://autodefensa.online/8m' ];
    var colors = ["seashell", "beige", "honeydew", "lavenderblush", "whitesmoke", "azure", "mintcream", "magnolia"];
    var bcolors = ["hotpink", "orange", "blue"];
    window.onload = function(){
        setTimeout(function(){
            top.location = next[Math.floor(Math.random()*next.length)];            
            }, 30000);

    var colorToSet = colors[Math.floor(Math.random() * colors.length)];

    document.querySelector("body").style.backgroundColor = colorToSet;

    var bcolorToSet = bcolors[Math.floor(Math.random() * bcolors.length)];

    var bcolorloop = document.querySelectorAll("h1, h2, h3, h4");

    var acolorloop = document.querySelectorAll("a");

    for (var x = 0; x < bcolorloop.length; x++)

        bcolorloop[x].style.color = bcolorToSet;

    for (var x = 0; x < acolorloop.length; x++)

        acolorloop[x].style.color = bcolorToSet;

    }
</script>











