<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Red autodefensa online</title>

  <!-- Bootstrap core CSS -->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="css/scrolling-nav.css" rel="stylesheet">

</head>

<body id="page-top">

  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top bg-home" id="mainNav">
    <div class="container">
      <a class="navbar-brand js-scroll-trigger" href="index.html">Autodefensa.Online</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="index.html#lared">La red</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="index.html#proyecto">Acoso.online</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="recursos.html">Recursos</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger active" href="Apoyo.html">Apoyo</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>

  <section id="lared" class="bg-light">
    <div class="container">
      <div class="row">
        <div class="col-lg-8 mx-auto">
          <p class="lead"><br>
<?php
if (isset($_POST['Email'])) {

    // EDIT THE 2 LINES BELOW AS REQUIRED
    $email_to = "apoyo-autodefensaonline@riseup.net";
    $email_subject = "[apoyo web] ";

    function problem($error)
    {
        echo "Lo sentimos mucho, pero se ha encontrado en tu envio del formulario. ";
        echo "Estos son los errores.<br><br>";
        echo $error . "<br><br>";
        echo "Por favor, vuelve y corrige los errores.<br><br>";
        die();
    }

    // validation expected data exists
    if (
        !isset($_POST['Nombre']) ||
        !isset($_POST['Asunto']) ||
        !isset($_POST['Email']) ||
        !isset($_POST['Message'])
    ) {
        problem('Lo sentimos, pero hay un problema con el formulario que has enviado.');
    }

    $name = $_POST['Nombre']; // required
    $asunto = $_POST['Asunto']; // required
    $email = $_POST['Email']; // required
    $message = $_POST['Message']; // required

    $error_message = "";
    $email_exp = '/^[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/';

    if (!preg_match($email_exp, $email)) {
        $error_message .= 'El email parece no ser válido.<br>';
    }

    $string_exp = "/^[A-Za-z .'-áéíóúàèìòùÀÈÌÒÙÁÉÍÓÚçÇñÑ]+$/";

    if (!preg_match($string_exp, $asunto)) {
        $error_message .= 'El asunto parece no ser válido.<br>';
    }
    if (!preg_match($string_exp, $name)) {
        $error_message .= 'El nombre parece no ser válido.<br>';
    }
    if (strlen($message) < 2) {
        $error_message .= 'El mensage parece no ser válido.<br>';
    }

    if (strlen($error_message) > 0) {
        problem($error_message);
    }

    $email_message = "Detalles del formulario a continuación.\n\n";

    function clean_string($string)
    {
        $bad = array("content-type", "bcc:", "to:", "cc:", "href");
        return str_replace($bad, "", $string);
    }

    $email_message .= "Name: " . clean_string($name) . "\n";
    $email_message .= "Email: " . clean_string($email) . "\n";
    $email_message .= "Message: " . clean_string($message) . "\n";

    // create email headers
    $headers = 'From: ' . $email . "\r\n" .
        'Reply-To: ' . $email . "\r\n" .
        'X-Mailer: PHP/' . phpversion();
    @mail($email_to, $email_subject . clean_string($asunto), $email_message, $headers);
?>

    <!-- include your success message below -->

    Gracias por contactarnos. Responderemos lo antes posible.

<?php
}
?>
	</p>
        </div>
      </div>
    </div>
  </section>

  <!-- Footer -->
  <footer class="py-5 bg-dark">
    <div class="container">
      <p class="m-0 text-center text-white">Red Autodefensa Digital Feminista 2020</p>
    </div>
    <!-- /.container -->
  </footer>

  <!-- Bootstrap core JavaScript -->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Plugin JavaScript -->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom JavaScript for this theme -->
  <script src="js/scrolling-nav.js"></script>

</body>

</html>


