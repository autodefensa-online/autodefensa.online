
  function closeAlert(){
    $(".alert-navegar").remove();
  }
  
  
  
  //fast exit button
$( document ).ready(function() {
//(function() {

var query=window.location.search.substring(1) ;
//si ja ha tancat, no mostrar alerta
if(query=="cerrar"){
    $(".alert-navegar").remove();
//mostrar alerta
}else{
    $(".alert-navegar").css("display","block");
}
    // forEach pollyfill for IE browsers
    if(window.NodeList && !NodeList.prototype.forEach) {
        NodeList.prototype.forEach = Array.prototype.forEach;
    }
    if(window.HTMLCollection && !HTMLCollection.prototype.forEach) {
        HTMLCollection.prototype.forEach = Array.prototype.forEach;
    }

    const hotKey = 27;
    // Fake site list
    const fakeSites = [
        'https://www.wikipedia.org/',
        'http://google.com/',
        'https://www.rtve.es/television/',
        'https://www.telecinco.es/',
        'https://www.lecturas.com/recetas/pollo-al-horno/'
    ];

    // Get random site from the fake site list
    function getRandomSite() {
        return fakeSites[Math.floor(Math.random() * fakeSites.length)];
    }

    function closeWin() {
        // Clear DOM to tackle with slow internet connection
        document.title = 'No se puede cargar la página';
        document.body.innerHTML = 'No se puede cargar la página';

        // Open random pages
        window.open(getRandomSite());
        setTimeout(function () {
            document.location.replace(getRandomSite());
        }, 100);
    }
    
    function initQuickExit() {
        // Change default anhor tag behaviour to replace history item
        const anchors = document.querySelectorAll('a');
        anchors.forEach(function(anchor) {
            anchor.addEventListener('click', function (e) {
                // Disable the defualt behaviour of redirecting to link
                e.preventDefault();
                e.returnValue = false; // IE support

                // Relpace the location - remove current url from history and disable back button
                var dire = anchor.href;
                //no tiene anchor
                if(dire.search("#") == -1 ){
                    //no tiene querystring
                    if (dire.search("cerrar") == -1 ){
                        dire= dire + "?cerrar";
                    }
                }else{
                    dire1 = dire.split("#");
                    if (dire.search("cerrar") == -1 ){
                            dire = dire1[0]+ "?cerrar#"+ dire1[1];
                    }else{
                        dire = dire1[0]+ "#"+ dire1[1];
                    }           
                }
                document.location.replace(dire);
            });
        });

        // Replace current page from history with random site
         document.querySelector('.salir-rapido').addEventListener('click', function () {
            closeWin();
        });

        document.addEventListener('keydown', function(e) {
            if(e.keyCode === hotKey) {
                closeWin();
                if(e) {
                    e.preventDefault();
                    e.returnValue = false; // IE support
                }
                return false;
            }
        });

    }

    initQuickExit();
//})();
});

/**
  function closeWin() { 
    window.location.replace('https://google.com');
    return false;     
  } 
  
  **/
